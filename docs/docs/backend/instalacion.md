# Instalación
## Paso 1
	Clonar el proyecto:
		git clone git@bitbucket.org:no2developer/skelsus-zend-site
		git clone https://<username>@bitbucket.org/no2developer/skelsus-zend-site 

Al terminar el paso 1 se tendrá la siguiente estructura
	project-dir-name

![Estructura](/docs/images/estructura/estructura.png   "Ejemplo estructura del ZF Skel")

## Paso 2
	Ir a la carpeta backend

## Paso 3
	Instalar composer:
		curl -s https://getcomposer.org/installer | php --

## Paso 4
	Instalar dependencias:
		php composer.phar install

## Paso 5
	Crear base de datos
		mysql -u<username> -p<password> 
			CREATE DATABASE project_dir_name_db DEFAULT CHARACTER SET utf8

## Paso 6
	Renombrar el archivo config/autoload/local.txt a
	config/autoload/local.php y cambiar las variables para el ambiente local.
		
	* accesos a la base de datos

		<database> = Nombre de la base de datos
		<username> = Nombre del usuario de base de datos
		<password> = Password del usuario de base de datos

		'db' => array(
			'driver' => 'Pdo',
			'dsn' => 'mysql:dbname=<database>;host=127.0.0.1',
			'driver_options' => array(
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
			),
			'username' => '<username>',
			'password' => '<password>',
		)

## Paso 7
	Ejecutar el siguiente comando
		vendor\bin\phinx init
	Este comando creará un archivo llamado phinx.yml

## Paso 8
	Abrir el archivo phinx.yml y cambiar los accesos de base de datos de acuerdo al ambiente en que estemos:
	<database> = Nombre de la base de datos
	<username> = Nombre del usuario de base de datos
	<password> = Password del usuario de base de datos
	-name: <database>
	-user: <username>
	-pass: <password>

## Paso 9
	Ejecutar las migraciones de base de datos:
	vendor\bin\phinx migrate -e [enviroment]	

