
Un mixin es un utilitario o helper que nos en términos de programación nos ayuda a reutilizar código de manera fácil y estandarizada. En nuestro flujo frontend lo aplicamos para formularios, para embeber etiquetas o para embeber código. Cabe mencionar que los mixins también pueden recibir parámetros.

## Mixins web

###  Mixin social

Éste mixin es utilizado para poder cargar las librerias JS de las redes sociales básicas: Facebook, Twitter y Google+. 

Éste mixin recibe 1 parámetro del tipo Array, en el cual debemos indicar las redes sociales que deseamos incrustar, a continuación los ejemplos:

Si deseamos incrustar solo Facebook, el código sería:

    +social(['fb'])

 Y si desearamos agregar Facebook y Twitter sería:

    +social(['fb', 'tw'])

###  Mixin tracking

Es utilizado para embeber el código UA de Google Analytics. No recibe ningún argumento, para su correcto funcionamiento es necesario que se haya configurado previamente el atributo **analytics_ua** dentro del archivo **base.js**, el cual se encuentra en:  

    ├── frontend
    │   ├── site
    │   │   ├── settings
    │   │   │   ├── base.js

Para usar éste mixin solo se coloca

    +tracking

Y por lo general esta ubicado en **_layout.jade**, en la ruta

    ├── frontend
    │   ├── site
    │   │   ├── templates
    │   │   │   ├── _layout.jade

###  Mixin requireTag

Éste mixin requiere de un parámetro que es el nombre de nuestro archivo JS que deseamos incluir en nuestro Jade.

Es importante mencionar que en éste mixin además de incluir nuestro modulo de Require, se incluye todo el archivo require-config como si fuera Javascript inline.

Su forma de uso es la siguiente:

    +requireTag('site.home')

## Mixins para Formularios

Para el uso de mixins en formularios, existen mixins especiales para cada tipo de input. La lista es la siguiente:

- input_text(config)
- input_date(config)  
- input_number(config)  
- input_email(config)    
- checkbox(config)   
- select(config)  


Cada uno recibe por parámetro un objeto “**config**” con las propiedades de cada mixin. Las propiedades pueden variar entre uno y otro, sin embargo existen algunas propiedades en común, a continuación las propiedades detalladas:


### input_text  
  Éste mixin se utiliza para colocar un input del tipo text. Sus atributos son los siguientes:

  - name
  > valor por defecto = ""  
    Esta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - var_name
  > valor por defecto = name
    Esta propiedad define el nombre de la variable en backend que representa al input.

  - id
  > valor por defecto = name  
    Esta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - placeholder
  > valor por defecto = ""  
    Esta propiedad asigna un placeholder al input.

  - tabindex
  > valor por defecto = "" 
    Esta propiedad asigna un tabindex al input.
  
  - defaultValue
  > valor por defecto = "" 
    Esta propiedad indica el valor por defecto que tendrá el input.

  - isDisabled
  > valor por defecto = "" 
    Esta propiedad indica si el input esta deshabilitado o no.

  - label
  > valor por defecto = "" 
    Esta propiedad indica cual es texto para la etiqueta del input.

  - required
  > valor por defecto = false
    Esta propiedad indica si el input es requerido o no.
  
  - attrClass
  > valor por defecto = ""
    Esta propiedad indica la clases correspondientes para el input.

  - minlength
  > valor por defecto = 0
    Esta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.

  - maxlength
  > valor por defecto = 100
    Esta propiedad asigna la cantidad máxima de caracteres con las que debe contar el input.

### input_number  

  Este mixin genera un input que solo permite el ingreso de números. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Esta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.
  
  - var_name
  > valor por defecto = name
    Esta propiedad define el nombre de la variable en backend que representa al input.

  - id
  > valor por defecto = name  
    Esta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.

  - placeholder
  > valor por defecto = ""  
    Esta propiedad asigna un placeholder al input.

  - tabindex
  > valor por defecto = "" 
    Esta propiedad asigna un tabindex al input.

  - defaultValue
  > valor por defecto = ""  
    Esta propiedad indica el valor por defecto que tendrá el input.

  - isDisabled
  > valor por defecto = "" 
    Esta propiedad indica si el input esta deshabilitado o no.

  - label
  > valor por defecto = "" 
    Esta propiedad indica cual es texto para la etiqueta del input.

  - required
  > valor por defecto = false
    Esta propiedad indica si el input es requerido o no.

  - attrClass
  > valor por defecto = ""
    Esta propiedad indica la clases correspondientes para el input.

  - minlength
  > valor por defecto = 0
    Esta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - minlength
  > valor por defecto = 0
    Esta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.

  - maxlength
  > valor por defecto = 100
    Esta propiedad asigna la cantidad máxima de caracteres con las que debe contar el input.

### input_date  
  Éste mixin generea un input del tipo date, el cual solo permite el ingreso de fechas. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Esta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - var_name
  > valor por defecto = name
    Esta propiedad define el nombre de la variable en backend que representa al input.

  - id
  > valor por defecto = name  
    Esta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - placeholder
  > valor por defecto = ""  
    Esta propiedad asigna un placeholder al input.

  - tabindex
  > valor por defecto = "" 
    Esta propiedad asigna un tabindex al input.
  
  - defaultValue
  > valor por defecto = "" 
    Esta propiedad indica el valor por defecto que tendrá el input.

  - isDisabled
  > valor por defecto = "" 
    Esta propiedad indica si el input esta deshabilitado o no.

  - label
  > valor por defecto = "" 
    Esta propiedad indica cual es texto para la etiqueta del input.

  - required
  > valor por defecto = false
    Esta propiedad indica si el input es requerido o no.
  
  - attrClass
  > valor por defecto = ""
    Esta propiedad indica la clases correspondientes para el input.

  - dateFormat
  > valor por defecto = 'dd/mm/yyyy'  
    Esta propiedad especifica el formato de fecha que se utilizará, por defecto es día, mes y año.
  
  - minDate
  > valor por defecto = '01/01/1920'  
    Especifica una fecha mínima aceptada, éste valor debe respetar el formato de fecha especificado anteriormente.
  
  - maxDate
  > valor por defecto = false
    Especifica una fecha máxima aceptada. Por defecto no existe éste límite.


### input_email  

  Éste mixin genera un input que solo permite el ingreso de correos electrónicos. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Esta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - var_name
  > valor por defecto = name
    Esta propiedad define el nombre de la variable en backend que representa al input.

  - id
  > valor por defecto = name  
    Esta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - placeholder
  > valor por defecto = ""  
    Esta propiedad asigna un placeholder al input.

  - tabindex
  > valor por defecto = "" 
    Esta propiedad asigna un tabindex al input.
  
  - defaultValue
  > valor por defecto = "" 
    Esta propiedad indica el valor por defecto que tendrá el input.

  - isDisabled
  > valor por defecto = "" 
    Esta propiedad indica si el input esta deshabilitado o no.

  - label
  > valor por defecto = "" 
    Esta propiedad indica cual es texto para la etiqueta del input.

  - required
  > valor por defecto = false
    Esta propiedad indica si el input es requerido o no.
  
  - attrClass
  > valor por defecto = ""
    Esta propiedad indica la clases correspondientes para el input.

  - minlength
  > valor por defecto = 0
    Esta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.

  - maxlength
  > valor por defecto = 100
    Esta propiedad asigna la cantidad máxima de caracteres con las que debe contar el input.

### checkbox  

  Éste mixin genera un input del tipo checkbox, a diferencia de **list_checkbox**, éste mixin se utiliza comunmente para el checkbox de términos y condiciones. Sus atributos son los siguientes.  

  - name
  > valor por defecto = ""  
    Esta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Esta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.

  - tabindex
  > valor por defecto = "" 
    Esta propiedad asigna un tabindex al input.
    
  - label
  > valor por defecto = ""  
    Esta propiedad indica el valor del label para el input.

  - link_text  
  > valor por defecto = ""  
    Aquí va el texto que estará enlazando a la url declarada en el atributo anterior

  - link_href  
  > valor por defecto = ""  
    Aquí se indica la url del link a donde queremos apuntar. Por lo general es la url de términos y condiciones.

  - has_link  
  > valor por defecto = false  
    Indica si el label de nuestro checkbox tendrá algún enlace o no.

  - attrClass
  > valor por defecto = ""
    Esta propiedad indica la clases correspondientes para el input.

### select   
  
  Éste mixin genera un input del tipo select. Sus atributos son los siguientes.  

  - name
  > valor por defecto = ""  
    Esta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Esta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.  

  - var_name
  > valor por defecto = name
    Esta propiedad define el nombre de la variable en backend que representa al input.

  - required
  > valor por defecto = false
    Esta propiedad indica si el input es requerido o no.

  - tabindex
  > valor por defecto = "" 
    Esta propiedad asigna un tabindex al input.
  
  - valueSelected
  > valor por defecto = false  
    Esta propiedad asigna un valor por defecto.

  - isDisabled
  > valor por defecto = false  
    Esta propiedad indica si el input está deshabilitado o no.

  - hasNoLabel
  > valor por defecto = false  
    Esta propiedad indica si el input no tiene **label**
  
  - label
  > valor por defecto = ""  
    Esta propiedad indica el valor del label para el input.  

  - defaultText
  > valor por defecto = ""  
    Esta propiedad asigna un texto por defecto. Esta propiedad es usualmente utilizada cuando el input select ha sido personalizado.

  - attrClass
  > valor por defecto = ""
    Esta propiedad indica la clases correspondientes para el input.


