
Actualmente se encuentran 12 tareas disponibles, cada tarea se encuentra en un archivo separado.

** Estructura **

    ├── frontend
    │   ├── site
    │   │   ├── tasks
    │   │   │   ├── copy.js
    │   │   │   ├── cssmin.js
    │   │   │   ├── deploy.js
    │   │   │   ├── htmlmin.js
    │   │   │   ├── notify.js
    │   │   │   ├── requirejs.js
    │   │   │   ├── server.js
    │   │   │   ├── smushit.js
    │   │   │   ├── sprites.js
    │   │   │   ├── styles.js
    │   │   │   ├── templates.js
    │   │   │   ├── watch.js

El archivo Gruntfile.js carga todas las tareas para que puedan ser instanciadas desde la consola.

    grunt.loadTasks('./tasks'); 



## Copy

**Paquete de dependencia:** 

grunt-contrib-copy

**Descripción:**

Copia archivos de la carpeta frontend hacia la carpeta backend/public, se usa en el caso de fuentes, imágenes u otros assets.

    module.exports = function(grunt) {
      // Carga el paquete de dependencia
      grunt.loadNpmTasks('grunt-contrib-copy');

      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de las subtareas: images y fonts.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación.
      grunt.config.set('copy', {
        images: { // Copia imágenes
          files: [{
            expand: true, // Recorre todos los archivos de la carpeta definida
            cwd: 'static/images', // Define la ruta de origen de los archivos
            src: ['*', '**/*'], //  Define que tipo de archivos seran copiados
            dest: grunt.config.get('config').deploy_routes().images // Define la ruta de salida de los archivos
          }]
        },
        fonts: { // Copia fuentes
          files: [{
            expand: true, // Recorre todos los archivos de la carpeta definida
            cwd: 'static/fonts', // Define la ruta de origen de los archivos
            src: ['*', '**/*'], //  Define que tipo de archivos seran copiados
            dest: grunt.config.get('config').deploy_routes().fonts // Define la ruta de salida de los archivos
          }]
        }
      });
    }



## Cssmin

**Paquete de dependencia:** 

grunt-contrib-cssmin

**Descripción:**

Minifica el css.

    module.exports = function(grunt) {
      var dest;
      
      // Define la ruta destino de los archivos css minificados
      dest = grunt.config.get('config').deploy_routes().styles;
      // Carga el paquete de dependencia
      grunt.loadNpmTasks('grunt-contrib-cssmin');
      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
      grunt.config.set('cssmin', {
        compile: {
          files: [{
            expand: true, // Recorre todos los archivos de la carpeta definida
            cwd: dest,  // Define la ruta de origen de los archivos
            src: ['*.css'], //  Define que tipo de archivos seran copiados (solo archivos .css).
            dest: dest, // Define la ruta destino de los archivos
            keepSpecialComments: false // Define si debe o no conservar los comentarios.
          }]
        }
      });
    }



## Deploy

**Paquete de dependencia:** 

grunt-contrib-cssmin

**Descripción:**

Ejecuta todas las tareas.

    module.exports = function(grunt) {  
      // Si el enviroment es dev(desarrollo) el task deploy ejecuta las tareas:
      // templates, styles, sprites
      if (grunt.option('dev')) {
        grunt.registerTask('deploy', 'Deployando el proyecto en desarrollo', function() {
          grunt.task.run(['templates', 'styles', 'sprites']);
        });
      } else {
        // Si el enviroment no esta especificado se asume que se trabaja en prod(producción) 
        // el task deploy ejecuta las tareas: templates, htmlmin, styles, cssmin, sprites y 
        // copy_assets y scripts.
        grunt.registerTask('deploy', 'Deployando en proyecto en produccion', function() {
          grunt.task.run([
            'templates',
            'htmlmin',
            'styles:compile',
            'cssmin:minify',
            'sprites',
            'copy_assets',
            'scripts'
          ]);
        });
      }
    };


## Htmlmin

**Paquete de dependencia:** 

grunt-contrib-htmlmin

**Descripción:**

Minifica el html.

    module.exports = function(grunt) {
      var dest;
      // Define la ruta destino de los archivos html minificados
      dest = grunt.config.get('config').deploy_routes().templates;
      // Carga el paquete de dependencia
      grunt.loadNpmTasks('grunt-contrib-htmlmin');
      
      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
      grunt.config.set('htmlmin', {
        compile: {
          options: { // Configura las opciones de minificado                        
            removeComments: true, // Define si debe o no eliminar los comentarios.
            collapseWhitespace: true, // Define si debe eliminar los espacios en blanco
            minifyJS: true // Define si debe minificar el javascript inline
          },
          files: [
            {
              expand: true, // Recorre todos los archivos de la carpeta definida
              cwd: dest, // Define la ruta de origen de los archivos
              src:'**/*' + grunt.config.get('config').settings.template_ext, //  Define que tipo de archivos seran copiados.
              dest: dest  // Define la ruta de salida de los archivos
            }
          ]
        }
      });
    };

## Notify
**Paquete de dependencia:** 

grunt-notify

**Descripción:**

Envia notificaciones al finalizar una determinada tarea.

*Windows 7 o < : Requiere instalación de Growl

    module.exports = function(grunt) {
      // Carga el paquete de dependencia
      grunt.loadNpmTasks('grunt-notify');

      grunt.config.set('notify', {
        watch: { // Notificación para tarea watch
          options: {
            title: 'Watch',
            message: 'Updated files are compiled'
          }
        },
        connect: { // Notificación para tarea connect
          options: {
            title: 'Conect',
            message: 'Static server is ready'
          }
        },
        sprites: { // Notificación para tarea sprites
          options: {
            title: 'Glue',
            message: 'Sprites generated'
          }
        },
        templates: { // Notificación para tarea templates
          options: {
            title: 'Templates',
            message: 'Jade compiled'
          }
        },    
        copy_assets: { // Notificación para tarea copy_assets
          options: {
            title: 'Copy',
            message: 'Images, sprites and fonts copied'
          }
        },
        scripts: { // Notificación para tarea scripts
          options: {
            title: 'Requirejs',
            message: 'Requirejs modules compiled'
          }
        },
        styles: { // Notificación para tarea styles
          options: {
            title: 'Css',
            message: 'Stylus compiled',
          }
        }
      });
    };


## RequireJS

**Paquete de dependencia:** 

grunt-requirejs

**Descripción:**

Compila y minifica los módulos requirejs

    module.exports = function(grunt) {
      // Carga el paquete de dependecia
      grunt.loadNpmTasks('grunt-contrib-requirejs');

      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
      grunt.config.set('requirejs', {
        compile: {
          options: {
            // Ruta de origen de todos los módulos require
            appDir:'static/scripts',
            // Ruta de configuración de las dependencias de los módulos js
            mainConfigFile: "static/scripts/libs/require-config.js",
            // Define url base para la carga de dependencia de los módulos
            baseUrl: '.',
            // Define ruta destino de compilación de los módulos
            dir: grunt.config.get('config').deploy_routes().scripts,
            // Define si debe o no conservar los comentanrios de las licencias
            preserveLicenseComments: false,
            // Define los módulos principales a compilar
            modules: [
              { name: 'site.home' },
              { name: 'site.tyc' },
              { name: 'site.prehome' },
              { name: 'site.register' },
              { name: 'site.thanks' }
            ]
          }
        }
      });

      // Registra tarea scripts que ejecuta la tarea requirejs
      // y la subtarea notif:sprites
      grunt.registerTask('scripts',['requirejs','notify:scripts']);
    };

## Server

**Paquete de dependencia:** 

connect

**Descripción:**

Servidor de archivos estáticos, evita el deploy total de la aplicación para
cargar archivos estáticos.

    // Declara variables globales
    var connect,
        grunt;

    // Carga paquete connect (servidor estático)
    connect = require('connect');

    // Define función para el servidor estático, recibe parametros opcionales 
    // de host y puerto donde correra el servidor.

    function staticServer(host, port) {
      var done,
          host,
          port,
          con;  

      done = this.async(); // Define que la tarea es asincrona
      host = host || 'localhost'; // Define el host, por defecto es localhost
      port = port || 8080; // Define el puerto, por defecto es 8080
      con = connect(); // Ejecuta función connect

      // Configura la ruta de archivos de carga para la url estática de scripts
      con.use(grunt.config.get('config').static_uri() + '/scripts', connect.static('static/scripts'));
      // Configura la ruta de archivos de carga para la url estática de estilos(css)
      con.use(grunt.config.get('config').static_uri() + '/styles', connect.static(grunt.config.get('config').deploy_routes().styles));
      // Configura la ruta de archivos de carga para la url estática de sprites
      con.use(grunt.config.get('config').static_uri() + '/sprites', connect.static(grunt.config.get('config').deploy_routes().sprites));
      // Configura la ruta de archivos de carga para la url estática que no sean 
      // ninguna de las rutas anteriores
      con.use(grunt.config.get('config').static_uri(), connect.static('static'));

      // Si el enviroment es dev(desarrollo) carga la url raiz "/" de la ruta 
      // de archivo de los templates
      if (grunt.config.get('config').getEnv() === 'dev') {
        con.use('/', connect.static(grunt.config.get('config').deploy_routes().base + '/templates'));
      }

      // Configura el servidor estatico con el puerto y host definido
      con.listen(port, host);
      // Muestra mensaje en la consola
      grunt.log.write('\nStarting static web server in "%s" on port %s.', host, port);  
    }

    module.exports = function(g) {  
      grunt = g;
      // Registra tarea connect que ejecuta la función staticServer
      grunt.registerTask('connect',  staticServer);
    };

## Smushit

**Paquete de dependencia:** 

grunt-smushit

**Descripción:**

Comprime imágenes sin perder calidad. Utiliza un servicio externo.

    module.exports = function(grunt) {
      var dest;
      // Define la ruta destino de los archivos html minificados
      dest = grunt.config.get('config').deploy_routes().images;
      // Carga el paquete de dependecia
      grunt.loadNpmTasks('grunt-smushit');
      
      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
      grunt.config.set('smushit', {
        optimize: {     
          expand: true, // Recorre todos los archivos de la carpeta definida
          cwd: 'static/images',  // Define la ruta de origen de los archivos
          src: ['**/*.jpg', '**/*.png'], 
          dest: grunt.config.get('config').deploy_routes().images  // Define la ruta de salida de los archivos    
        }
      });
    };


## Sprites

**Paquete de dependencia:** 

glue

**Descripción:**

Crea sprites y su css respectivo.

    module.exports = function(grunt) {

      var config,
          dest;

      // Define la variable config a partir de la propiedad config de grunt
      config = grunt.config.get('config');
      // Define la ruta destino de los sprites y su css
      dest = config.deploy_routes().sprites;
      // Carga librería grunt glue
      grunt.loadNpmTasks('grunt-glue');

      grunt.config.set('glue', {
        compile: {
          src: 'static/sprites', // Define ruta de los archivos de origen
          options: '--css=' + dest + // Define ruta destino del css
            ' --namespace=sp' + // Define namespace para selectores
            ' --img=' + dest + // Define ruta de salida para la imagen concantenada
            ' --url=../sprites/ --margin=10' + // Define la ruta de referencia de los sprites para el css y el margen entre imagenes
            ' --recursive' + // Recorre todo las carpetas de la ruta de origen
            ' --project '// Genera cada sprite de carpeta en imágenes separadas
        }
      }); 
      // Define tarea sprites que ejecuta a las tareas glue, stylus:glue y notify:sprites
      grunt.registerTask('sprites', 'Compiling Templates', function() {
        grunt.task.run(['glue', 'stylus:glue', 'notify:sprites']);
      });

    };

## Styles

**Paquete de dependencia:** 

grunt-contrib-stylus

**Descripción:**

Compila archivos stylus.

    module.exports = function(grunt) {
      // Carga el paquete de dependecia
      grunt.loadNpmTasks('grunt-contrib-stylus');

      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile y glue.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
       grunt.config.set('stylus', {
        compile: {
          options: {
            compress: true // Define si debe o no comprimir el css generado
          },
          files: [{
            expand: true, // Recorre todos los archivos de la carpeta definida
            cwd: 'static/styles', // Define la ruta de origen de los archivos
            src: [
              '**.styl' //  Define que tipo de archivos seran compilador (solo archivos .styl).
            ],
            dest: grunt.config.get('config').deploy_routes().styles,  // Define la ruta destino de los archivos
            ext:'.css'
          }]
        },
        glue: {
          options: {
            '-C': true // Definir compilación de los archivo a formato stylus
          },
          files: [{
            expand: true, // Recorre todos los archivos de la carpeta definida
            cwd: grunt.config.get('config').deploy_routes().sprites,  // Define la ruta de origen de los archivos
            src: [
              '*.css' //  Define que tipo de archivos seran compilador (solo archivos .css).
            ],
            dest: 'static/styles/modules', // Define la ruta destino de los archivos compilados
            ext: '.sprite.styl' // Define la extensión del archivo generado
          }]
        }
      });

      // Registra tarea styles que ejecuta en el enviroment dev(desarrollo)
      // la subtareas stylus:compile y notify:styles
      // y en el enviroment prod(producción ) ejecuta las subtareas
      // stylus:compile, cssmin:minify y notify:styles
      grunt.registerTask('styles', 'Compiling Templates', function() {
        if (grunt.option('dev')) {
          grunt.task.run(['stylus:compile', 'notify:styles']);
        } else {
          grunt.task.run(['stylus:compile', 'cssmin:compile', 'notify:styles']);
        }
      });
    };

## Templates

**Paquete de dependencia:** 

grunt-contrib-jade

**Descripción:**
Compila archivos jade


    module.exports = function(grunt) {
      // Carga el paquete de dependecia
      grunt.loadNpmTasks('grunt-contrib-jade');

      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
      grunt.config.set('jade', {
        compile: {
          options: {
            pretty: false, // Define si el html debe salir formateado o no
            data: { // Define variables internas disponibles en jade
              config : grunt.config.get('config')  // Asigna variable config con la configuración de grunt
            }
          },
          files: [
            {
              expand: true,  // Recorre todos los archivos de la carpeta definida
              cwd: 'templates/sections', // Define la ruta de origen de los archivos
              //  Define que tipo de archivos seran compilados(los ! son excepciones)
              src: [
                '*.jade',
                '**/*.jade',
                '!_layout.jade',
                '!**/_layout.jade',
                '!includes/**/*.jade',
                '!mixins/**/*.jade',
                '!_*.jade'
              ],
               // Define la ruta destino de los archivos
              dest: grunt.config.get('config').deploy_routes().templates,
              ext: grunt.config.get('config').settings.template_ext// Define la extensión del archivo generado
            }
          ]
        }
      });

      // Registra tarea templates que ejecuta en el enviroment dev(desarrollo)
      // la subtareas jade y notify:templates
      // y en el enviroment prod(producción ) ejecuta las subtareas
      // jade, htmlmin y notify:templates   
      grunt.registerTask('templates', 'Compiling Templates', function () {
        if (grunt.option('format')) {
          grunt.config.set('jade.compile.options.pretty', true);
        }

        if (grunt.option('dev')) {
          grunt.config.set('jade.compile.files.0.ext', '.html');
        } else {
          if (grunt.option('format')) {
            grunt.task.run(['jade', 'notify:templates']);     
          } else {
            grunt.task.run(['jade', 'htmlmin', 'notify:templates']);
          }
        }    
      });
    };

## Watch

**Paquete de dependencia:** 

grunt-contrib-jade

**Descripción:**
Ejecuta tareas al encontrar cambios en archivos .styl o .jade


    module.exports = function(grunt) {
      // Carga el paquete de dependecia
      grunt.loadNpmTasks('grunt-contrib-watch');

      // Configura la tarea enviando como primer parámetro el nombre de la tarea y 
      // como segundo la configuración interna de la subtarea:compile.
      // Ojo: El nombre de la tarea debe ser el mismo que el de su documentación
      grunt.config.set('jade', {
        compile: {
          options: {
            pretty: false, // Define si el html debe salir formateado o no
            data: { // Define variables internas disponibles en jade
              config : grunt.config.get('config')  // Asigna variable config con la configuración de grunt
            }
          },
          files: [
            {
              expand: true,  // Recorre todos los archivos de la carpeta definida
              cwd: 'templates/sections', // Define la ruta de origen de los archivos
              //  Define que tipo de archivos seran compilados(los ! son excepciones)
              src: [
                '*.jade',
                '**/*.jade',
                '!_layout.jade',
                '!**/_layout.jade',
                '!includes/**/*.jade',
                '!mixins/**/*.jade',
                '!_*.jade'
              ],
               // Define la ruta destino de los archivos
              dest: grunt.config.get('config').deploy_routes().templates,
              ext: grunt.config.get('config').settings.template_ext// Define la extensión del archivo generado
            }
          ]
        }
      });

      // Registra tarea templates que ejecuta en el enviroment dev(desarrollo)
      // la subtareas jade y notify:templates
      // y en el enviroment prod(producción ) ejecuta las subtareas
      // jade, htmlmin y notify:templates   
      grunt.registerTask('templates', 'Compiling Templates', function () {
        if (grunt.option('format')) {
          grunt.config.set('jade.compile.options.pretty', true);
        }

        if (grunt.option('dev')) {
          grunt.config.set('jade.compile.files.0.ext', '.html');
        } else {
          if (grunt.option('format')) {
            grunt.task.run(['jade', 'notify:templates']);     
          } else {
            grunt.task.run(['jade', 'htmlmin', 'notify:templates']);
          }
        }    
      });
    };